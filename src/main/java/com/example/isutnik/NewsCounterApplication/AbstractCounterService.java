package com.example.isutnik.NewsCounterApplication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Pattern;

@Configuration
public abstract class AbstractCounterService implements NewsCounterService{

    @Value("${pattern}")
    public Pattern pattern;
}
