package com.example.isutnik.NewsCounterApplication;

import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractDownloadService {
    @Value("${URL}")
    public String URL;
}
