package com.example.isutnik.NewsCounterApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import java.io.IOException;


@SpringBootApplication
public class NewsCounterApplication {
	public static void main(String[] args) throws IOException {
		final ApplicationContext run = SpringApplication.run(NewsCounterApplication.class, args);
		final NewsCounterUcs bean = run.getBean(NewsCounterUcs.class);
		bean.run();

	}

}
