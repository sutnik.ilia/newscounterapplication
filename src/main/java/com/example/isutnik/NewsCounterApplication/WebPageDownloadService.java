package com.example.isutnik.NewsCounterApplication;

import java.io.IOException;

public interface WebPageDownloadService {
     String download() throws IOException;
}
