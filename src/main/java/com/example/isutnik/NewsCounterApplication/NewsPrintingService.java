package com.example.isutnik.NewsCounterApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NewsPrintingService {

    public NewsPrintingService(AbstractDownloadService abstractDownloadService) {
        this.abstractDownloadService = abstractDownloadService;
    }

    final AbstractDownloadService abstractDownloadService;

    void printNewsCount(int count) throws IOException {
        System.out.println("The amount of today's news on " + abstractDownloadService.URL+ " is " + count);
    }
}
