package com.example.isutnik.NewsCounterApplication;

public interface NewsCounterService {
    int count(final String page);
}
