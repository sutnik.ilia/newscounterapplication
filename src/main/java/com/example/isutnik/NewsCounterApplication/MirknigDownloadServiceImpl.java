package com.example.isutnik.NewsCounterApplication;

import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Profile("book")
@Component
public class MirknigDownloadServiceImpl extends AbstractDownloadService implements WebPageDownloadService{
    @Override
    public String download() throws IOException {
        return Request.Post(URL)
                .setHeader("User-Agent", "MySuperUserAgent")
                .execute()
                .returnContent()
                .asString();
    }
}
