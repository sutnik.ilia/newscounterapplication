package com.example.isutnik.NewsCounterApplication;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Profile("book")
@Component
public class MirknigNewsCounterServiceImpl extends AbstractCounterService implements NewsCounterService {
    @Override
    public int count(final String page) {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(String.valueOf(pattern));
        final Pattern patternNews1 = Pattern.compile("</a> " + LocalDate.now().format(dateTimeFormatter));
        final Matcher matcher = patternNews1.matcher(page);
        return (int) Stream.of(matcher)
                .map(Matcher::find)
                .count();
    }
}
